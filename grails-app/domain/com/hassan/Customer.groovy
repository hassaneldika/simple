package com.hassan

class Customer{

  String name
  String email
  String address
  String phone
 

  static constraints = {
    email email: true
  }
}